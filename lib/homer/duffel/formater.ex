defmodule Homer.Duffel.Formater do

  def format_offers(%{"data" => %{"offers" => offers}}), do: offers |> Enum.map(&format_offer/1)
  def format_offers(error), do: {:error, error}

  # Note: the API payload doesn't make any sense. There is too much data, some duplicated.
  # They should use GraphQL
  def format_offer(%{"slices" => [%{"segments" => [segment | _]} = slice | _]} = offer) do
    duration = Timex.Parse.Duration.Parsers.ISO8601Parser.parse(segment["duration"])
    |> elem(1)
    |> Timex.Duration.to_minutes(truncate: true)

    %{
      arriving_at: segment["arriving_at"],
      departing_at: segment["departing_at"],
      destination: segment["destination"]["name"],
      origin: segment["origin"]["name"],
      segments_count: length(slice["segments"]),
      total_amount: offer["total_amount"],
      total_duration: duration
    }
  end
end
