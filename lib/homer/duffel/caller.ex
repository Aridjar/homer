defmodule Homer.Duffel.Caller do
  @moduledoc """
  Every API call to Duffel should use this module.
  """
  require Logger

  alias Homer.Search.OfferRequest

  @uri "https://api.duffel.com/"
  @header [
    # "Accept-Encoding": gzip,
    "Accept": "application/json",
    "Duffel-Version": "beta",
    "Authorization": "Bearer #{Application.fetch_env!(:homer, :duffel_api_key)}",
    "Content-Type": "application/json"
  ]

  ################
  ### Airports ###
  ################

  @spec get_airports([], String.t) :: []
  def get_airports(previous_data \\ [], next_page \\ "") do
    url = gen_url("air/airports?limit=200", next_page)

    with {:ok, response} <- HTTPoison.get(url, @header)
    do
      data = Jason.decode!(response.body)
      airport_count = length(data["data"])

      get_next_airports(previous_data, data, airport_count)
    else
      {:error, message} -> Logger.info("Duffel get airport: #{inspect(message)}" )
    end
  end

  @spec get_next_airports([], %{}, integer) :: []
  defp get_next_airports(previous_data, %{"data" => data, "meta" => meta}, 200), do:
    previous_data ++ data |> get_airports(meta["after"])

  defp get_next_airports(previous_data, %{"data" => data}, _), do: previous_data ++ data

  ################
  ### Airlines ###
  ################

  @spec get_airlines([], String.t) :: []
  def get_airlines(previous_data \\ [], next_page \\ "") do
    url = gen_url("air/airlines?limit=200", next_page)

    with {:ok, response} <- HTTPoison.get(url, @header)
    do
      data = Jason.decode!(response.body)
      airlines_count = length(data["data"])

      get_next_airlines(previous_data, data, airlines_count)
    else
      {:error, message} -> Logger.info("Duffel get airport: #{inspect(message)}" )
    end
  end

  @spec get_next_airlines([], %{}, integer) :: []
  defp get_next_airlines(previous_data, %{"data" => data, "meta" => meta}, 200), do:
    previous_data ++ data |> get_airlines(meta["after"])

  defp get_next_airlines(previous_data, %{"data" => data}, _), do: previous_data ++ data

  ##############
  ### Offers ###
  ##############

  @spec find_offers(OfferRequest.t()) :: {:ok, [Offer.t()]} | {:error, any()}
  def find_offers(request_params) do
    url = gen_url("air/offer_requests?return_offers=true")
    body = gen_body(request_params)

    with {:ok, response} <- HTTPoison.post(url, body, @header)
    do
      response.body
      |> Jason.decode!()
      |> Homer.Duffel.Formater.format_offers()
    else
      {:error, message} -> Logger.info("Duffel get airport: #{inspect(message)}" )
    end
  end

  @spec gen_body(%OfferRequest{}) :: String.t
  defp gen_body(%OfferRequest{departure_date: d_date, origin: origin, destination: destination}) do
    %{
      data: %{
        slices: [
          %{
            origin: origin,
            destination: destination,
            departure_date: d_date |> Date.to_iso8601()
          }
        ],
        passengers: [
          %{
            type: "adult"
          },
        ],
        cabin_class: "economy"
      }
    }
    |> Jason.encode!()
  end

  #########################
  ### General functions ###
  #########################

  @spec gen_url(String.t, String.t) :: String.t
  defp gen_url(ressource, next_page \\ "")
  defp gen_url(ressource, ""), do: @uri <> ressource
  defp gen_url(ressource, next_page), do: "#{gen_url(ressource)}&after=#{next_page}"
end
