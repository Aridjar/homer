defmodule Homer.Duffel.AirportLibrary do
  @moduledoc """
    The Genserver is here to avoid calling to often the API to get airports.
    This is definitly not the best solution: it would have been better to save them in DB, as
      - It's unlikely to have frequent changes, or new airport every days.
      - If the server need to restart, the data would already be in DB and therefor no call would be needed
      - The DB allow us to not have more to configure if we need to had server.
    But, I didn't want to over extend the exercice to much, as I wanted to have an accurate airport list
    with the possibility to actualise this list at a given time.

    There is also no test associated to this Module, though it should be possible thanks to Erlang Trace
    https://www.thegreatcodeadventure.com/testing-genservers-with-erlang-trace/
  """

  use GenServer

  alias Homer.Duffel.Caller

  def start_link(_) do
    GenServer.start_link(__MODULE__, %{}, name: :airport_library)
  end

  @impl true
  def init(_) do
    timer = Application.fetch_env!(:homer, :airport_actualization_timer)

    new_state = %{
      timer: timer,
      airports: [],
      airlines: [],
      init_completed: false
    }

    {:ok, new_state}
  end

  ##########################
  ### External functions ###
  ##########################

  @doc """
  Retrieve all airports listed in the :airport_library GenServer

  ## Examples

    iex> get_airports()
    [%{id: "arp_lhr_gb", iata_code: "LHR", name: "Heathrow"}, %{...}, ...]

  """
  @spec get_airports() :: []
  def get_airports() do
    GenServer.call(:airport_library, :get_airports)
  end

  @spec get_airlines() :: []
  def get_airlines() do
    GenServer.call(:airport_library, :get_airlines)
  end

  @impl true
  def handle_call(:get_airports, _from, %{airports: airports} = state) do
    {:reply, airports, state}
  end

  @impl true
  def handle_call(:get_airlines, _from, %{airlines: airlines} = state) do
    {:reply, airlines, state}
  end

  ######################
  ### Initialization ###
  ######################

  def complete_initialization() do
    GenServer.cast(:airport_library, :complete_init)
  end

  @impl true
  def handle_cast(:complete_init, %{init_completed: true} = state), do: {:noreply, state}

  @impl true
  def handle_cast(:complete_init, state) do
    new_state = %{update_lib(state) | init_completed: true}

    IO.puts "App ready"

    schedule_api_call(new_state)

    {:noreply, new_state}
  end

  ##################
  ### Chron work ###
  ##################

  defp schedule_api_call(%{timer: timer}), do: Process.send_after(self(), :update_lib, timer)

  @impl true
  def handle_info(:update_lib, state) do
    new_state = update_lib(state)
    schedule_api_call(new_state)

    {:noreply, new_state}
  end

  defp update_lib(state) do
    task_airports = Task.async(&(Caller.get_airports/0))
    task_airlines = Task.async(&Caller.get_airlines/0)

    airports = Task.await(task_airports)
    |> Enum.map(&({&1["city_name"], &1["iata_code"]}))

    airlines = Task.await(task_airlines)
    |> Enum.map(&(&1["name"]))

    %{state | airports: airports, airlines: airlines}
  end
end
