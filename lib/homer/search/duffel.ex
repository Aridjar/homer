defmodule Homer.Search.Duffel do
  alias Homer.{Offers, Search.Offer}

  @behaviour Homer.Search.ProviderBehaviour

  @spec fetch_offers(OfferRequest.t()) :: {:ok, [Offer.t()]} | {:error, any()}
  def fetch_offers(offer_request) do
    offers = Homer.Duffel.Caller.find_offers(offer_request)
    |> Enum.map(&Offers.create_offer/1)
    |> Enum.map(&(elem(&1, 1)))

    {:ok, offers}
  end
end
