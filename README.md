# Homer <!-- omit in toc -->

This project is a simplified version of the existing services provided by [Ulysse](https://odyssey.ulysse.com/) to compare plane ticket in real time.

The main difference is that it only use the API endpoints provided by [Duffel](https://duffel.com/), and doesn't have an advanced design.

It uses **Elixir 1.12.3** as well as **Phoenix 1.6.2**, as well as **LiveView 0.16.4**. Read more in (#Dependencies)[#dependencies]

## Summary

- [Summary](#summary)
- [Installation](#installation)
  - [Docker && Postgres](#docker--postgres)
  - [Elixir](#elixir)
- [Running](#running)
  - [Postgres](#postgres)
  - [Elixir](#elixir-1)
- [Changing exercices](#changing-exercices)
- [Technical choices](#technical-choices)
  - [Airlines and Airport](#airlines-and-airport)
  - [Duffel folder](#duffel-folder)
- [Specificities](#specificities)
- [Dependencies](#dependencies)
- [Work time](#work-time)
  - [Time loss](#time-loss)
- [Answer for exercice 6, 7, 9 and the 2 last questions](#answer-for-exercice-6-7-9-and-the-2-last-questions)
  - [Exercice 6 : Genserver + timer](#exercice-6--genserver--timer)
    - [Sorting](#sorting)
  - [Exercice 7](#exercice-7)
  - [Exercice 9](#exercice-9)
  - [Questions](#questions)

## Installation

### Docker && Postgres

Copy the `.env.dist` to a folder named `.env`

Replace the content

```bash
docker-compose build
# the build can take some time. Once it's done, launch the DB to check if it works as well as setup ecto.

docker-compose up
```

### Elixir

```elixir
mix deps.get

# Note: you'll need the DB up to execute this command.
mix ecto.setup
```

Set the DUFFEL_API_KEY in your local variable or 

- go to the `config/config.exs` file
- uncomment the `duffel_api_key: [YOUR_API_KEY]` line
- comment the next one
- replace [YOUR_API_KEY] by your API key

## Running

### Postgres

Open two clis, one for each command
```bash
docker-compose up

docker-compose exec postgres psql [username] [role]
```

> Note: the default values for dev are set to docker and docker, as well as the password.
> If you want to change them, you will also have to change them in the `config/dev.exs` file.

### Elixir

```elixir
mix phx.server

# If you want the cli
iex -S phx.server
```

Now you can visit [localhost:4000](localhost:4000)

## Changing exercices

To compare each version of the execices, you can either change branch or change to the wanted commit through the log.

Here are the branches names

- 2_gen_livewiew

## Technical choices

### Airlines and Airport

As there is no indication on how we should handle or get the airlines name and airport iata_code, I choose to create a GenServer to get them with a regular update and save them in cache.

It's not the best solution, as there is few chances any data change on a hourly basis. Also, I could have save them in DB, but that would have mean create new schemas. Then the correct way would have been to create association between offers, airlines, airports and offer request, which I didn't want to get into as it can be really cumburstone.

There is no reason to use the 9k airports and 1k1 airlines directly to the data. If all, it make the process update slow. A search module creating a option list after the first 3 chars would have been the best thing to do, but it was once again going too far from the exercice.

### Duffel folder

I prefer to go for a folder per API, even if this exercice use only 1. This allows to separate the . Though, each folder should have common files

- Caller (from which all calls are emited). If it become to important, most elements such as the body generator should be moved to another file.
- Formater (transforming the data format from the API to our format)

Both should implement a behaviour, allowing to dynamically call any functions.

## Specificities

Some function use @spec. I didn't add that many @doc or @moduledoc as most of the elements are generate from Elixir.

There is no test on the 

## Dependencies

- [Elixir 1.12.3](https://elixir-lang.org/)
- [Phoenix 1.6.2](https://www.phoenixframework.org/)
  - [Phoenix Live View 0.16.4](https://hexdocs.pm/phoenix_live_view/Phoenix.LiveView.html)

## Work time

- start at 16:58
- pause at 18:21, restart at 18:25
- pause at 19:25, restart at 19:31
- pause at 19:55, restart at 20:15
- pause at 21:50, restart at 22:39
- pause at 23:44, restart at 23:48
- Stop at 00:36

- start at 12h12
- pause at 14:49, restart at 15:23
- end at 15:53

total time : 9h22

### Time loss

- API (offer endpoint) : 1h30
- select_date => 30 minutes before using persistence : no other existing means were found to convert an keyword list to a date where the numbers are string.
- Move the form from the modal to the index => 45 minutes

- Non mandatory element
  - 1h30 to Setup the airports recuperation (but gained 45 to 1hour afterward)

Could have made what I have done in 6 hours.

## Answer for exercice 6, 7, 9 and the 2 last questions

### Exercice 6 : Genserver + timer

As Offer didn't included the airlines, I didn't see any way to filter with the actual saved offer. Therefore a new API call to Duffel should be made to exclude them from the accepted offer if we keep the define structure.

#### Sorting

The sort in the `Offers` module with a 

```elixir
def get_offer_ordered_by!(id, order) do
  Offer
  |> order_by(order)
  |> limit(10)
  |> Repo.all()
end
```

The other way is to use Enum to sort avec a custom function

```elixir
def sort_offers(key, offers), do: offers |> Enum.sort(&(&1[key] < &2[key]))

# if the previous doesn't work for whatever reason, we can still do as follow
def sort_offers(:total_amount, offers), do: offers |> Enum.sort(&(&1.total_amount < &2.total_amount))
def sort_offers(:total_duration, offers), do: offers |> Enum.sort(&(&1.total_duration < &2.total_duration))
```

###  Exercice 7

Add another function as follow

```elixir
  @timer == 15 * 60 * 1000

  def handle_cast(:any_process, _from, %{timer_reference: timer_reference} = state)
    timer_reference = kill_genserver_after_time(self())
    {:no_reply, %{state | timer_reference: timer_reference }
  end

  defp kill_genserver_after_time(pid, timer_reference) do
    Process.cancel_timer(timer_reference)
    Process.send_after(pid, :kill, @timer)
  end
```

Note that it also could be implemented in a `handle_continue` function, though it would make the timer restart after the logic execution and not before, which may create a problem.

### Exercice 9

Already done, should just call the get_offer_ordered_by instead of all offer, but that's it.

Also, I believe there is a way to add a limit to the heex `for`, but I couldn't find any information.

### Questions

> Given than in production we can have more than 5000 offers for one offer request. What persistence strategy do you suggest for offers ? Explain why.

First instinct was to use Redis, as offers could change, and they could also be access by other request depending of multiple element (airport proximity, date...), though I hesitated a lot with MongoDB (without any association of some kind) or PostgresQL

After 20 to 30 minutes of research, I reconcideder for the following answer

If there is 10_000 request in one day, it's can go up to ~580 offer per second to request, with pikes at more than 2k offer per seconds, which can be handle by PostgresQL.

Then, it depends of the strategy : either have an `Offer` table or put them in an `offer` JSONB column of an `OfferRequest` row.

If there is heavy work to do with time, a time serie DB such as timescale would be to used in complementarity.

Finally, should be stored in cache the most redundante request and offers.

> We now want to deploy the app we just created on multiple servers that are connected together using distributed erlang. Which parts of the code will require an update and why ?

Definitly not sure about this one. For me, the Genserver will require an update. The reason lies as a Genserver is isolated from other nodes, and if there is store data in the state that are valuable, if the node stop, these data are loss.

They should be store on an agent which should be shared between multiple nodes. This idea is based on the following talk: https://www.youtube.com/watch?v=nLApFANtkHs
